from __future__ import absolute_import
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"^(?P<name>\w+)/(?P<timeslot>\d{12})/product/$",
        views.get_product,
        name="get_product"),
    url(r"^(?P<name>\w+)/(?P<timeslot>\d{12})/quicklook/$",
        views.get_quicklook,
        name="get_global_quicklook"),
    url(r"^(?P<name>\w+)/(?P<timeslot>\d{12})/(?P<tile>\w+)/$",
        views.get_tiled_product,
        name="get_tiled_product"),
    url(r"^(?P<name>\w+)/(?P<tile>)\w+/(?P<timeslot>\d{12})/quicklook/$",
        views.get_quicklook,
        name="get_quickook"),
    url(r"^ogc/latest/$",
        views.get_latest_wms,
        name="get_latest_wms"),
    url(r"^legacy/LST/(?P<area_tile>\w+)/(?P<timeslot>\d{12})/$",
        views.get_legacy_lst,
        name="get_legacy_lst"),
    url(r"^legacy/LST/(?P<area_tile>\w+)/(?P<timeslot>\d{12})/quicklook/$",
        views.get_legacy_quicklook,
        name="get_legacy_quicklook"),
    url(
        "^orders/(?P<user_name>\w+)/"
        "order_(?P<order_id>\d+)/"
        "(?P<item_id>\w+)/"
        "(?P<file_name>[\w.]+)/$",
        views.get_ordered_file,
        name="get_ordered_file"
    ),
]
