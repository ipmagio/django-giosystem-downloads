"""
Retrieve product files from giosystem
"""

import datetime as dt
import logging
import os.path
import shutil
import urlparse

import pysftp

logger = logging.getLogger(__name__)

DATA_ROOTS = [os.path.expanduser(p) for p in
              os.getenv("CGLOPS_DATA_ROOTS", "/data").split(":")]

class CglopsProduct(object):
    """This is a different but simpler implementation of GioFile."""

    name = ""
    search_patterns = None
    timeslot = None

    def __init__(self, name, search_patterns=None, timeslot=None):
        """Create a new CglopsProduct instance.

        Parameters
        ----------
        name: str
            A unique identifier for this object. It should be a human-readable
            string. However it should contain only alphanumeric characters plus
            the underscore.
        search_urls: list
            An iterable with search_urls to use when there is a need to fetch
            the resources that this object represents. Each search_url should
            be a string parseable by the `urlparse.urlparse()` function. In
            addition, each search_url may have format expressions that can be
            used to specify properties of the object. Example:

                sftp://<user>:<password>@<host>/{data_roots[0]}/outputs/lst/{timeslot:%Y}/{timeslot:%m}/{timeslot:%d}/g2_BIOPAR_LST_{timeslot:%Y%m%d%H%M_GLOBE_GEO_V1.2.nc}

            The variables that can be used in each search_url are:

            * `data_roots` - A list with the directories that are considered
              the base directories where data is found on the host
            * All of the object's properties, such as `timeslot` and `name`
        timeslot: datetime.datetime
            The temporal slot that the object represents

        """

        self.name = name
        self.timeslot = timeslot or dt.datetime.utcnow()
        self.search_patterns = search_patterns or []

    def __repr__(self):
        return ("{0.__class__.__name__}(name={0.name!r}, "
                "search_patterns={0.search_patterns!r}, "
                "timeslot={0.timeslot!r})".format(self))

    @property
    def search_urls(self):
        for pattern in self.search_patterns:
            url = urlparse.urlparse(
                pattern.format(data_roots=DATA_ROOTS, **self.__dict__)
            )
            yield url

    @property
    def local_search_urls(self):
        for url in self.search_urls:
            if url.scheme == "file":
                yield url

    @property
    def remote_search_urls(self):
        for url in self.search_urls:
            if url.scheme != "file":
                yield url

    def fetch(self, destination, remote_search=False):
        parsed_destination = os.path.abspath(
            os.path.expandvars(
                os.path.expanduser(destination)
            )
        )
        destination_dir = os.path.dirname(parsed_destination)
        if not os.path.isdir(destination_dir):
            os.makedirs(destination_dir)
        urls = self.search_urls if remote_search else self.local_search_urls
        for url in urls:
            logger.debug("Trying URL: {}".format(url.geturl()))
            try:
                if url.scheme == "file":
                    result = self._fetch_local(url.path, parsed_destination)
                else:
                    result = self._fetch_remote(url, parsed_destination)
                break
            except Exception as err:
                logger.warning("Error when fetching from url "
                               "{0!r}: {1}".format(url.geturl(), err))
                pass
        else:
            try:
                os.removedirs(destination_dir)
            except OSError:  # directory not empty
                pass
            raise RuntimeError("Could not fetch {}".format(self))
        return result

    def _fetch_local(self, path, destination, force=True):
        if force:
            try:
                os.remove(destination)
            except OSError:  # file is not there
                pass
        shutil.copy(path, destination)
        return destination

    def _fetch_remote(self, url, destination):
        if url.scheme == "sftp":
            with pysftp.Connection(url.hostname, username=url.username,
                                   password=url.password) as sftp, \
                    pysftp.cd(destination):
                sftp.get(url.path)
        else:
            raise NotImplementedError("{0:!r} scheme is not implemented")


PRODUCTS = {
    "lst": (
        CglopsProduct,
        [
            "file://{data_roots[0]}/outputs/lst/{timeslot:%Y}/{timeslot:%m}/"
            "{timeslot:%d}/g2_BIOPAR_LST_{timeslot:%Y%m%d%H%M}_"
            "GLOBE_GEO_V1.2.nc",
        ],
    ),
}


def get_retriever_from_path(path):
    name, slot = path.split("_")[2:4]
    timeslot = dt.datetime.strptime(slot, "%Y%m%d%H%M")
    ProductClass, search_patterns = PRODUCTS.get(name.lower(), (None, None))
    return ProductClass(
        name=name.lower(),
        search_patterns=search_patterns,
        timeslot=timeslot
    )
