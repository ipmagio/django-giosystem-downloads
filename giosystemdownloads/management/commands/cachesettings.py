"""
This script caches giosystem's settings from an online source. The cached
settings are used when responding to requests made to the 
django-giosystem-downloads app.
"""

import os
import sys
import pickle

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings as django_settings

import giosystemcore.settings


class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        settings_manager = giosystemcore.settings.get_settings(
                django_settings.GIOSYSTEM_SETTINGS_URL)
        settings_manager.get_all_packages_settings()
        settings_manager.get_all_files_settings()
        settings_manager.get_all_hosts_settings()
        settings_manager.get_all_products_settings()
        settings_manager.get_all_areas_settings()
        settings_manager.get_all_sources_settings()
        settings_manager.get_all_organizations_settings()
        settings_manager.get_all_palettes_settings()
        pickle_name = 'pickledsettings.pkl'
        pickle_path = os.path.join(django_settings.BASE_DIR, pickle_name)
        with open(pickle_path, 'wb') as fh:
            pickle.dump(settings_manager, fh)
            sys.stdout.write('settings have been cached to {}\n'.format(
                             pickle_path))
