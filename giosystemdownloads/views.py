"""
Test the upload using httpie with a request like the following:

http --verbose --form PUT
  "http://localhost:8000/downloads/api/internal/lst african tile/201409150100/"
  @/home/ricardo/Desktop/teste.txt
  Content-Type:*/*
  Accept:application/vnd.giosystem+file
  Content-Disposition:"attachment; filename=teste.txt"
"""

import pickle
import os
import re
import shutil
import tempfile

from django.http import HttpResponse, Http404
from django.conf import settings as django_settings
import mapscript
from sendfile import sendfile

from djangomapserver import ows
import giosystemcore.errors
from giosystemcore.hosts.hostfactory import get_host
from giosystemcore import files
import giosystemcore.orders.singledownloads as sd
import giosystemcore.settings
from giosystemcore import utilities
from giosystemcore.tools.mapserver import mapserver
from giosystemordering.orderprocessor import OrderProcessor

from . import productretriever

_AREA_NAME_PART = {
    "AFRI": "african",
    "ASIA": "asian",
    "EURO": "european",
    "NOAM": "north american",
    "SOAM": "south american",
    "OCEA": "oceanian",
}


def get_ordered_file(request, user_name, order_id, item_id, file_name):
    retriever = productretriever.get_retriever_from_path(file_name)
    temp_dir = tempfile.mkdtemp()
    try:
        # TODO - Authenticate and authorize user!
        path = retriever.fetch(temp_dir)
        # TODO - Update oseoserver with the download info
        #        This will require the implementation of an API for oseoserver
        result = sendfile(request, path,
                          attachment=True,
                          attachment_filename=os.path.basename(path),
                          #mimetype=_get_mime_type(path),
                          encoding="utf8")
    except Exception:
        result = Http404()
    finally:
        shutil.rmtree(temp_dir)
    return result


def old_get_ordered_file(request, user_name, order_id, item_id, file_name):
    """Handle delivery of order items that specify HTTP as method."""

    try:
        # 1. TODO - Authenticate and authorize user!

        # 2. Convert the url into a file system path
        full_url = request.build_absolute_uri()
        full_url = full_url[:-1] if full_url.endswith('/') else full_url
        item_processor = OrderProcessor()
        path = item_processor.get_file_path(full_url)

        # 3. TODO - Update oseoserver with the download info
        #    This will require the implementation of an API for oseoserver

        # 4. Deliver the file
        result = sendfile(request, path,
                          attachment=True,
                          attachment_filename=os.path.basename(path),
                          #mimetype=_get_mime_type(path),
                          encoding="utf8")
    except Exception:
        result = Http404()
    return result


def get_tiled_product(request, name, timeslot, tile):
    name = name.upper()
    tile = tile.upper()
    giosystemcore.settings._settings_manager = _unpickle_settings()
    dp = sd.DownloadPreparator(name, timeslot, tile=tile)
    the_zip = dp.get_zip()
    response = HttpResponse(content_type='application/zip')
    with open(the_zip) as fh:
        response.write(fh.read())
    response['Content-Disposition'] = 'attachment; filename={}'.format(
            os.path.basename(the_zip))
    return response


def get_product(request, name, timeslot):
    """Retrieve a global product from the input arguments"""
    name = name.upper()
    giosystemcore.settings._settings_manager = _unpickle_settings()
    gio_files = files.get_product_files(name, timeslot, file_type="netcdf")
    pattern = "_".join((name, timeslot))
    try:
        gio_file = [f for f in gio_files if pattern in f.search_pattern][0]
    except IndexError:
        raise giosystemcore.errors.OutputNotFoundError
    else:
        host = get_host()
        output_path = os.path.join(host.data_dir, gio_file.search_path)
        fetched = gio_file.fetch(
            output_path,
            use_archive=getattr(django_settings, "GIOSYSTEM_USE_ARCHIVE",
                                False),
            use_io_buffer=getattr(django_settings, "GIOSYSTEM_USE_IO_BUFFER",
                                  False)
        )
        if fetched is not None:
            result = sendfile(request, fetched, attachment=True,
                              attachment_filename=os.path.basename(fetched),
                              mimetype=utilities.get_mimetype(fetched),
                              encoding="utf8")
        else:
            raise Http404("The requested file is not available")
    return result


def get_quicklook(request, name, timeslot, tile="GLOBE"):
    """Retrieve a quicklook file for the input product"""
    giosystemcore.settings._settings_manager = _unpickle_settings()
    settings_manager = giosystemcore.settings.get_settings()
    pattern = r"_{}_.*?_{}_".format(name.upper(), tile.upper())
    png_gio_files = (fs for fs in settings_manager._files.values() if
                     fs["file_type"] == "png")
    for file_settings in png_gio_files:
        if re.search(pattern, file_settings["search_pattern"]) is not None:
            gio_file = files.get_file(file_settings["name"], timeslot)
            host, found = gio_file.find(use_io_buffer=False,
                                        use_archive=False)
            try:
                path = gio_file.selection_rule.filter_paths(found)
            except IndexError:
                continue
            result = sendfile(request, path, attachment=False,
                              attachment_filename=os.path.basename(path),
                              mimetype=utilities.get_mimetype(path),
                              encoding="utf8")
            break
    else:
        raise Http404("The requested file is not available")
    return result


def get_latest_wms(request):
    giosystemcore.settings._settings_manager = _unpickle_settings()
    gio_file = files.get_file(
        "latest_products_mapfile",
        timeslot="201501010000"  # the timeslot is irrelevant for this file
    )
    found = gio_file.find(use_archive=False, use_io_buffer=False)
    try:
        path = found[1][0]
    except IndexError:
        raise Http404("The requested file is not available")
    else:
        os.environ["MS_DEBUGLEVEL"] = "5"
        processor = mapserver.MapfileProcessor.from_mapfile(path)
        map_obj = processor.get_mapfile()
        map_obj.setConfigOption("MS_ERRORFILE",
                                "/home/geo2/Desktop/mapserver_errors.txt")
        map_obj.setConfigOption("CPL_DEBUG", "ON")
        map_obj.setConfigOption("PROJ_DEBUG", "ON")
        map_obj.debug = mapscript.MS_DEBUGLEVEL_DEBUG
        result, content_type = ows.process_request(request, map_obj)
        response = HttpResponse(content_type=content_type)
        response.write(result)
    return response


def old_get_latest_wms(request):
    giosystemcore.settings._settings_manager = _unpickle_settings()
    gio_file = files.get_file(
        "latest_products_mapfile",
        timeslot="201501010000"  # the timeslot is irrelevant for this file
    )
    found = gio_file.find(use_archive=False, use_io_buffer=False)
    try:
        path = found[1][0]
    except IndexError:
        raise Http404("The requested file is not available")
    else:
        map_obj = mapscript.mapObj(path)
        result, content_type = ows.process_request(request, map_obj)
        response = HttpResponse(content_type=content_type)
        response.write(result)
    return response


def get_legacy_lst(request, area_tile, timeslot):
    """Return a legacy HDF5 tiled product."""
    try:
        area_name_part = _AREA_NAME_PART[area_tile]
    except KeyError:
        raise Http404
    gio_file_name = "lst {} tile".format(area_name_part)
    return _get_legacy_gio_file(request, gio_file_name, timeslot,
                                compress=True)


def get_legacy_quicklook(request, area_tile, timeslot):
    """Return a legacy quicklook for a tiled product."""
    try:
        area_name_part = _AREA_NAME_PART[area_tile]
    except KeyError:
        raise Http404
    gio_file_name = "lst {} quicklook".format(area_name_part)
    return _get_legacy_gio_file(request, gio_file_name, timeslot)


def _get_legacy_gio_file(request, name, timeslot, compress=False):
    # we are not using the pickled settings here
    giosystemcore.settings.get_settings(django_settings.GIOSYSTEM_SETTINGS_URL)
    gio_file = files.get_file(name, timeslot)
    host = get_host()
    output_path = os.path.join(host.data_dir, gio_file.search_path)
    fetched = gio_file.fetch(
        output_path,
        use_archive=getattr(django_settings, "GIOSYSTEM_USE_ARCHIVE",
                            False),
        use_io_buffer=getattr(django_settings, "GIOSYSTEM_USE_IO_BUFFER",
                              False),
        decompress=not compress
    )
    if fetched is not None:
        base = os.path.basename(fetched)
        if not base.endswith("bz2") and gio_file.file_type == "hdf5":
            file_name = (".".join((base, "h5")) if not base.endswith(".h5")
                         else base)
        else:
            file_name = base
        result = sendfile(request, fetched, attachment=True,
                          attachment_filename=file_name,
                          mimetype=utilities.get_mimetype(file_name),
                          encoding="utf8")
    else:
        raise Http404("The requested file is not available")
    return result


def _unpickle_settings():
    """
    Restore the settings that are cached in a pickle file back into memory.
    """

    pickle_dir = os.path.dirname(os.path.abspath(__file__))
    pickle_name = "pickledsettings.pkl"
    pickle_path = os.path.join(django_settings.BASE_DIR, pickle_name)
    try:
        with open(pickle_path, "rb") as fh:
            settings_manager = pickle.load(fh)
    except IOError as err:
        print("Could not find the cached settings.")
        raise
    return settings_manager
