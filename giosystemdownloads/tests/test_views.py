"""Unit and integration tests for application views."""

import os

import mock
from django.http import HttpResponse

from giosystemdownloads import views


def test_get_legacy_lst(client):
    timeslot = "201101121400"
    area = "EURO"
    expected_gio_file_name = "lst european tile"
    expected_response = HttpResponse("")
    with mock.patch("giosystemdownloads.views._get_legacy_gio_file",
                    autospec=True) as mock_get_gio_file:
        mock_get_gio_file.return_value = expected_response
        response = client.get("/downloads/legacy/LST/"
                              "{}/{}/".format(area, timeslot))
        called_with_args, called_with_kwargs = mock_get_gio_file.call_args
        assert called_with_args[1] == expected_gio_file_name
        assert called_with_args[2] == timeslot
        assert called_with_kwargs["compress"] == True
        assert response == expected_response


def test_get_legacy_quicklook(client):
    timeslot = "201101121400"
    area = "EURO"
    expected_gio_file_name = "lst european quicklook"
    expected_response = HttpResponse("")
    with mock.patch("giosystemdownloads.views._get_legacy_gio_file",
                    autospec=True) as mock_get_gio_file:
        mock_get_gio_file.return_value = expected_response
        response = client.get("/downloads/legacy/LST/"
                              "{}/{}/quicklook/".format(area, timeslot))
        called_with_args, called_with_kwargs = mock_get_gio_file.call_args
        assert called_with_args[1] == expected_gio_file_name
        assert called_with_args[2] == timeslot
        assert response == expected_response


def test_get_legacy_gio_file_get_lst():
    """Unit test for the views._get_legacy_gio_file private function."""

    timeslot = "fake_timeslot"
    gio_file_name = "fake_gio_file"
    gio_file_file_type = "hdf5"
    gio_file_search_path = ""
    host_data_dir = ""
    use_archive = True
    use_io_buffer = False
    fake_django_request = None
    fake_fetched = "fake/path/for/file.bz2"
    expected_file_name = "file.bz2"
    expected_mimetype = "application/x-bzip2"
    fake_response = HttpResponse("fake_result")

    settings_path = "giosystemdownloads.views.giosystemcore.settings"
    file_path = "giosystemdownloads.views.files"
    get_host_path = "giosystemdownloads.views.get_host"
    sendfile_path = "giosystemdownloads.views.sendfile"
    django_settings_path = "giosystemdownloads.views.django_settings"

    with mock.patch(settings_path, autospec=True) as mock_settings, \
            mock.patch(file_path, autospec=True) as mock_files, \
            mock.patch(get_host_path, autospec=True) as mock_get_host, \
            mock.patch(sendfile_path, autospec=True) as mock_sendfile, \
            mock.patch(django_settings_path) as mock_django_settings:
        mock_gio_file = mock.MagicMock(
            "giosystemdownloads.views.files.GioFile",
            autospec=True
        ).return_value
        mock_gio_file.search_path = gio_file_search_path
        mock_gio_file.file_type = gio_file_file_type
        mock_gio_file.fetch.return_value = fake_fetched
        mock_files.get_file.return_value = mock_gio_file
        mock_host = mock.MagicMock(
            "giosystemcore.hosts.hosts.GioLocalHost",
            autospec=True
        ).return_value
        mock_host.data_dir = host_data_dir
        mock_get_host.return_value = mock_host
        mock_sendfile.return_value = fake_response
        mock_django_settings.GIOSYSTEM_SETTINGS_URL = "fake"
        mock_django_settings.GIOSYSTEM_USE_ARCHIVE = use_archive
        mock_django_settings.GIOSYSTEM_USE_IO_BUFFER =  use_io_buffer

        response = views._get_legacy_gio_file(fake_django_request,
                                              gio_file_name, timeslot)

        mock_files.get_file.assert_called_with(gio_file_name, timeslot)
        mock_gio_file.fetch.assert_called_with(
            os.path.join(host_data_dir, gio_file_search_path),
            use_archive=use_archive,
            use_io_buffer = use_io_buffer,
            decompress=True
        )

        sendfile_args, sendfile_kwargs = mock_sendfile.call_args
        assert sendfile_args[0] == fake_django_request
        assert sendfile_args[1] == fake_fetched
        assert sendfile_kwargs["attachment"] == True
        assert sendfile_kwargs["attachment_filename"] == expected_file_name
        assert sendfile_kwargs["mimetype"] == expected_mimetype
        assert sendfile_kwargs["encoding"] == "utf8"

        assert response.content == fake_response.content
