.. note::

   This project is **deprecated**.
   The code has been moved and improved in the *giosystem-downloads*
   repository

This is a django app that serves file download requests for giosystem.

Installation
============

1. Create a virtualenv and activate it

   .. code:: bash

      virtualenv venv
      source venv/bin/activate

#. Install the following system-wide dependencies:

   .. code:: bash

      sudo apt-get install libxml2 libxml2-dev libxslt1.1 libxslt1-dev

#. Install lxml, which is required by pywps.

   .. code:: bash

      pip install lxml

#. Clone this repository into your working directory

   .. code:: bash

      git clone https://<your-user-account>@bitbucket.org/ipmagio/django-giosystem-downloads.git

#. Install the code in editable mode using pip

   .. code:: bash

      pip install --editable django-giosystem-downloads

#. Add it as an installed app in the django settings file

#. Add it to the `urls.py` file
